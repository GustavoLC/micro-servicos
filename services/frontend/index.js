function newDice(dice) {
    const div = document.createElement('div');
    div.className = 'column is-2';
    div.innerHTML = `
        <div class="card is-shady">
            <div class="card-image">
                <figure class="image is-4by3">
                    <img
                        src="${dice.photo}"
                        alt="${dice.name}"
                        class="modal-button"
                    />
                </figure>
            </div>
            <div class="card-content">
                <div class="content dice" data-id="${dice.id}">
                    <div class="dice-meta">
                        <h4 class="is-size-3 title">${dice.name}</h4>
                    </div>
                </div>
            </div>
        </div>`;
    return div;
}

function calculateSorting() {
    fetch('http://localhost:3000/sorting/')
        .then((data) => {
            console.log(data);
            if (data.ok) {
                return data.json();
            }
            throw data.statusText;
        })
        .then((data) => {
            swal('', `O dado sorteado foi o de número ${data.value}`, 'success');
        })
        .catch((err) => {
            swal('Erro', 'Erro ao consultar frete', 'error');
            console.error(err);
        });
}

document.addEventListener('DOMContentLoaded', function () {
    const dices = document.querySelector('.dices');

    fetch('http://localhost:3000/images')
        .then((data) => {
            console.log(data);
            if (data.ok) {
                return data.json();
            }
            throw data.statusText;
        })
        .then((data) => {
            if (data) {
                data.forEach((dice) => {
                    dices.appendChild(newDice(dice));
                });

                document.querySelectorAll('.button-sorting').forEach((btn) => {
                    btn.addEventListener('click', (e) => {
                        calculateSorting();
                    });
                });

                document.querySelectorAll('.button-buy').forEach((btn) => {
                    btn.addEventListener('click', (e) => {
                        swal('Compra de livro', 'Sua compra foi realizada com sucesso', 'success');
                    });
                });
            }
        })
        .catch((err) => {
            swal('Erro', 'Erro ao listar os produtos', 'error');
            console.error(err);
        });
});
