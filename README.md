# Dado Online - Exemplo de Micro-Serviço Simples

Este repositório contem um exemplo simples de um dado online constrúido utilizando de uma **arquitetura de microsserviços**.

O exemplo foi construido com base no seguinte repositório: [Micro-Livraria: Exemplo Prático de Microsserviços](https://github.com/aserg-ufmg/micro-livraria). A parte teórica e lógica do sistema está detalhada no documento citado.

Foi utilizado: **Node.js**, **REST**, **gRPC** e **Docker**.

## Executando o Sistema

1. Faça o Fork e o Clone do repositório

3. É também necessário ter o Node.js instalado na sua máquina. Se você não tem, siga as instruções para instalação contidas nessa [página](https://nodejs.org/en/download/).

4. Em um terminal, vá para o diretório no qual o projeto foi clonado e instale as dependências necessárias para execução dos microsserviços:

```
cd micro-servicos
npm install (ou yarn install, yarn é um gerenciador de pacotes)
```

5. Inicie os microsserviços através do comando:

```
npm run start ou yarn start
```

6. Teste agora o sistema, abrindo o front-end em um navegador: http://localhost:5000.

7. Instale o docker em sua máquina através [desse link](https://docs.docker.com/get-docker/)

8. Crie um arquivo na raiz do projeto com o nome `sorting.Dockerfile`. Este arquivo armazenará as instruções para criação de uma imagem Docker para o serviço `Sorting`.

No Dockerfile, você precisa incluir cinco instruções

-   `FROM`: tecnologia que será a base de criação da imagem.
-   `WORKDIR`: diretório da imagem na qual os comandos serão executados.
-   `COPY`: comando para copiar o código fonte para a imagem.
-   `RUN`: comando para instalação de dependências.
-   `CMD`: comando para executar o seu código quando o container for criado.

Ou seja, nosso Dockerfile terá as seguintes linhas:

```Dockerfile
# Imagem base derivada do Node
FROM node

# Diretório de trabalho
WORKDIR /app

# Comando para copiar os arquivos para a pasta /app da imagem
COPY . /app

# Comando para instalar as dependências
RUN npm install

# Comando para inicializar (executar) a aplicação
CMD ["node", "/app/services/sorting/index.js"]
```

9. Compile o Dockerfile para criar a imagem com o seguinte comando.

```
docker build -t micro-livraria/sorting -f sorting.Dockerfile ./
```
10. Remova a inicialização do serviço de Sorting do comando `npm run start`. Para isso, basta remover o sub-comando `start-sorting` localizado na linha 7 do arquivo *package.json*, conforme exemplo.

```diff
diff --git a/package.json b/package.json
index 25ff65c..552a04e 100644
--- a/package.json
+++ b/package.json
@@ -4,7 +4,7 @@
     "description": "Toy example of microservice",
     "main": "",
     "scripts": {
-        "start": "run-p start-frontend start-controller start-sorting start-inventory",
+        "start": "run-p start-frontend start-controller start-inventory",
         "start-controller": "nodemon services/controller/index.js",
         "start-sorting": "nodemon services/sorting/index.js",
         "start-inventory": "nodemon services/inventory/index.js",

```

11. Reinicie o sistema. Basta dar um CTRL+C no terminal e rodar o comando `npm run start` novamente.

12. Execute a imagem criada no passo anterior (ou seja, colocar de novo o microsserviço de `Sorting` no ar), basta usar o comando:

```
docker run -ti --name sorting -p 3001:3001 micro-servicos/sorting
```

onde:

-   `docker run`: comando de execução de uma imagem docker.
-   `-ti`: habilita a interação com o container via terminal.
-   `--name sorting`: define o nome do container criado.
-   `-p 3001:3001`: redireciona a porta 3001 do container para sua máquina.
-   `micro-livraria/sorting`: especifica qual a imagem deve-se executar.

Se tudo estiver correto, você irá receber a seguinte mensagem em seu terminal:

```
Sorting Service running
```

E, finalmente, o Controller pode acessar o serviço diretamente através do container Docker.