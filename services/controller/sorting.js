const grpc = require('@grpc/grpc-js');
const protoLoader = require('@grpc/proto-loader');

const packageDefinition = protoLoader.loadSync('proto/sorting.proto', {
    keepCase: true,
    longs: String,
    enums: String,
    arrays: true,
});

const SortingService = grpc.loadPackageDefinition(packageDefinition).SortingService;
const client = new SortingService('127.0.0.1:3001', grpc.credentials.createInsecure());

module.exports = client;
