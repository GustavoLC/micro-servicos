const express = require('express');
const sorting = require('./sorting');
const inventory = require('./inventory');
const cors = require('cors');

const app = express();
app.use(cors());

/**
 * Retorna a lista de produtos da loja via InventoryService
 */
app.get('/images', (req, res, next) => {
    inventory.SearchAllImages(null, (err, data) => {
        if (err) {
            console.error(err);
            res.status(500).send({ error: 'something failed :(' });
        } else {
            res.json(data.images);
        }
    });
});

/**
 * Consulta o frete de envio no SortingService
 */
app.get('/sorting/', (req, res, next) => {
    sorting.GetSortingRate(
        {
        },
        (err, data) => {
            if (err) {
                console.error(err);
                res.status(500).send({ error: 'something failed :(' });
            } else {
                res.json({
                    value: data.value,
                });
            }
        }
    );
});

/**
 * Inicia o router
 */
app.listen(3000, () => {
    console.log('Controller Service running on http://127.0.0.1:3000');
});
