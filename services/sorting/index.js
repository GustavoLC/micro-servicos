const grpc = require('@grpc/grpc-js');
const protoLoader = require('@grpc/proto-loader');

const packageDefinition = protoLoader.loadSync('proto/sorting.proto', {
    keepCase: true,
    longs: String,
    enums: String,
    arrays: true,
});

const sortingProto = grpc.loadPackageDefinition(packageDefinition);

const server = new grpc.Server();

// implementa os métodos do SortingService
server.addService(sortingProto.SortingService.service, {
    GetSortingRate: (_, callback) => {
        const sortingValue = Math.floor(Math.random() * 6) + 1; // Valor aleatório entre 1 e 6

        callback(null, {
            value: sortingValue,
        });
    },
});

server.bindAsync('0.0.0.0:3001', grpc.ServerCredentials.createInsecure(), () => {
    console.log('Sorting Service running at http://127.0.0.1:3001');
    server.start();
});
